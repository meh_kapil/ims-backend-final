module.exports = {
    server: '127.0.0.1',
    port: '9999',
    dbUrl: 'mongodb://127.0.0.1:27017',
    dbName: 'IMSFinalDB',
    dbUrlServer: '',
    jwtSecret: 'IMSFinalJWT',
    hash: 'bcryptHash',

}